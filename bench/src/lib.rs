#![feature(test)]
extern crate shortestpath;
extern crate test;

#[cfg(test)]
mod tests {
    use super::*;
    use shortestpath::mesh_2d::*;
    use shortestpath::mesh_3d::*;
    use shortestpath::*;
    use test::Bencher;

    #[bench]
    fn bench_full_2d_160x90(b: &mut Bencher) {
        let mesh = Full2D::new(160, 90);
        let mut grad = Gradient::new_with_mesh(&mesh);
        grad.set_distance(0, 0.0);
        b.iter(|| {
            grad.spread_forward(&mesh, 1.0);
        });
        println!("f: {}", repr_mesh_with_gradient_2d(&mesh, &grad));
    }

    #[bench]
    fn bench_compact_2d_160x90(b: &mut Bencher) {
        let mesh = Compact2D::new_full(160, 90);
        let mut grad = Gradient::new_with_mesh(&mesh);
        grad.set_distance(0, 0.0);
        b.iter(|| {
            grad.spread_backward(&mesh, 1.0);
        });
        println!("f: {}", repr_mesh_with_gradient_2d(&mesh, &grad));
    }

    #[bench]
    fn bench_full_3d_160x90x3(b: &mut Bencher) {
        let mesh = Full3D::new(160, 90, 3);
        let mut grad = Gradient::new_with_mesh(&mesh);
        grad.set_distance(0, 0.0);
        b.iter(|| {
            grad.spread_backward(&mesh, 1.0);
        });
        println!("f: {}", repr_mesh_with_gradient_3d(&mesh, &grad));
    }

    #[bench]
    fn bench_compact_3d_160x90x3(b: &mut Bencher) {
        let mesh = Compact3D::new_full(160, 90, 3);
        let mut grad = Gradient::new_with_mesh(&mesh);
        grad.set_distance(0, 0.0);
        b.iter(|| {
            grad.spread_forward(&mesh, 1.0);
        });
        println!("f: {}", repr_mesh_with_gradient_3d(&mesh, &grad));
    }
}

// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

use crate::errors::*;
use crate::mesh_topo::topology::*;

pub struct TopologyBool {
    status: bool,
}

impl TopologyBool {
    pub fn new(status: bool) -> Self {
        TopologyBool { status }
    }

    pub fn set(&mut self, status: bool) {
        self.status = status
    }

    pub fn get(&self) -> bool {
        self.status
    }
}

impl Topology for TopologyBool {
    fn allowed(&self, _from: usize, _to: usize) -> Result<bool> {
        Ok(self.status)
    }
}

impl From<bool> for TopologyBool {
    fn from(status: bool) -> TopologyBool {
        TopologyBool::new(status)
    }
}

impl From<TopologyBool> for bool {
    fn from(topo: TopologyBool) -> bool {
        topo.get()
    }
}

// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

use crate::errors::*;

pub trait Topology {
    fn allowed(&self, from: usize, to: usize) -> Result<bool>;
}

// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

use crate::gate::*;
use crate::mesh::*;
use crate::mesh_topo::topology::*;

#[derive(Clone)]
pub struct MeshWithTopology<'a, IntoIter>
where
    IntoIter: Iterator<Item = Gate>,
{
    mesh: &'a dyn Mesh<IntoIter = IntoIter>,
    topology: &'a dyn Topology,
}

impl<'a, IntoIter> MeshWithTopology<'a, IntoIter>
where
    IntoIter: Iterator<Item = Gate>,
{
    pub fn new(
        mesh: &'a impl Mesh<IntoIter = IntoIter>,
        topology: &'a impl Topology,
    ) -> MeshWithTopology<'a, IntoIter> {
        MeshWithTopology { mesh, topology }
    }
}

impl<'a, IntoIter> Mesh for MeshWithTopology<'a, IntoIter>
where
    IntoIter: Iterator<Item = Gate>,
{
    type IntoIter = std::vec::IntoIter<Gate>;

    fn successors(&self, from: usize) -> std::vec::IntoIter<Gate> {
        self.mesh
            .successors(from)
            .filter(|to| self.topology.allowed(from, to.target).unwrap_or(false))
            .collect::<Vec<Gate>>()
            .into_iter()
    }

    fn len(&self) -> usize {
        self.mesh.len()
    }
}

// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

//! The mesh_topo module contains tools to simulate some shape twists, such as
//! gates opening one-way, totally dynamic environments where the mesh
//! can change due to external conditions.

mod mesh_with_topology;
mod topology;
mod topology_bool;

pub use mesh_with_topology::*;
pub use topology::*;
pub use topology_bool::*;

// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

use crate::errors::*;
use crate::mesh_2d::*;

pub trait Index2D: Shape2D {
    fn index_to_xy(&self, index: usize) -> Result<(usize, usize)>;
    fn xy_to_index(&self, x: usize, y: usize) -> Result<usize>;
}

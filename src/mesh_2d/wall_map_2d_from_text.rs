// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

use crate::errors::*;
use crate::mesh_2d::shape_2d::*;
use crate::mesh_2d::wall_map_2d::*;

pub struct WallMap2DFromText {
    width: usize,
    data: Vec<Vec<char>>,
}

impl WallMap2DFromText {
    pub(crate) const DEFAULT_WALL_CHAR: char = '#';
    pub(crate) const FREE_CHARS: &'static str = " .o";
    pub(crate) const DEFAULT_FREE_CHAR: char = ' ';
    pub(crate) const SEP_CHARS: &'static str = "-_=";
    pub(crate) const DEFAULT_SEP_CHAR: char = '-';

    fn width_from_data(data: &Vec<Vec<char>>) -> usize {
        data.iter().map(|l| l.len()).max().unwrap_or(0)
    }

    pub fn get_char(&self, x: usize, y: usize) -> Result<char> {
        if y >= self.data.len() {
            return Err(Error::invalid_xy(x, y));
        }
        if x >= self.width {
            return Err(Error::invalid_xy(x, y));
        }
        let line = &self.data[y];
        if x >= line.len() {
            // If undef, it's not a wall, one can go there
            return Ok(Self::DEFAULT_FREE_CHAR);
        }
        Ok(line[x])
    }

    pub(crate) fn is_wall_char(chr: char) -> bool {
        Self::FREE_CHARS.find(chr).is_none() && !Self::is_sep_char(chr)
    }

    pub(crate) fn is_sep_char(chr: char) -> bool {
        Self::SEP_CHARS.find(chr).is_some()
    }

    pub fn new_with_str(input: &str) -> WallMap2DFromText {
        Self::new_with_iter_str(input.lines())
    }

    pub fn new_with_string(input: &String) -> WallMap2DFromText {
        Self::new_with_iter_str(input.lines())
    }

    pub fn new_with_vec_str(input: &Vec<&str>) -> WallMap2DFromText {
        Self::new_with_iter_str(input.iter().map(|l| *l))
    }

    pub fn new_with_vec_string(input: &Vec<String>) -> WallMap2DFromText {
        Self::new_with_iter_string(input.iter())
    }

    pub fn new_with_iter_str<'a, I>(input: I) -> WallMap2DFromText
    where
        I: Iterator<Item = &'a str>,
    {
        let data = input
            .map(|line| String::from(line).chars().collect())
            .collect();
        let width = Self::width_from_data(&data);
        WallMap2DFromText { width, data }
    }

    pub fn new_with_iter_string<'a, I>(input: I) -> WallMap2DFromText
    where
        I: Iterator<Item = &'a String>,
    {
        let data = input.map(|line| line.clone().chars().collect()).collect();
        let width = Self::width_from_data(&data);
        WallMap2DFromText { width, data }
    }
}

impl WallMap2D for WallMap2DFromText {
    fn is_wall(&self, x: usize, y: usize) -> Result<bool> {
        let chr = self.get_char(x, y)?;
        Ok(Self::is_wall_char(chr))
    }
}

impl Shape2D for WallMap2DFromText {
    fn shape(&self) -> (usize, usize) {
        (self.width, self.data.len())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_wall_map_2d_from_text_is_wall_char() {
        assert!(WallMap2DFromText::is_wall_char('#'));
        assert!(WallMap2DFromText::is_wall_char('a'));
        assert!(WallMap2DFromText::is_wall_char('*'));
        assert!(WallMap2DFromText::is_wall_char('/'));
        assert!(!WallMap2DFromText::is_wall_char('-'));
        assert!(!WallMap2DFromText::is_wall_char('_'));
        assert!(!WallMap2DFromText::is_wall_char('='));
        assert!(!WallMap2DFromText::is_wall_char(' '));
        assert!(!WallMap2DFromText::is_wall_char('.'));
        assert!(!WallMap2DFromText::is_wall_char('o'));
    }

    #[test]
    fn test_wall_map_2d_from_text_is_sep_char() {
        assert!(!WallMap2DFromText::is_sep_char('#'));
        assert!(!WallMap2DFromText::is_sep_char('a'));
        assert!(!WallMap2DFromText::is_sep_char('*'));
        assert!(!WallMap2DFromText::is_sep_char('/'));
        assert!(WallMap2DFromText::is_sep_char('-'));
        assert!(WallMap2DFromText::is_sep_char('_'));
        assert!(WallMap2DFromText::is_sep_char('='));
        assert!(!WallMap2DFromText::is_sep_char(' '));
        assert!(!WallMap2DFromText::is_sep_char('.'));
        assert!(!WallMap2DFromText::is_sep_char('o'));
    }

    #[test]
    fn test_wall_map_2d_from_text_new_with_str_basic() {
        let wall_map = WallMap2DFromText::new_with_str("aaaaaa\nb b b \n      \n123456");
        assert_eq!((6, 4), wall_map.shape());
        assert_eq!(Ok('a'), wall_map.get_char(0, 0));
        assert_eq!(Ok(true), wall_map.is_wall(0, 0));
        assert_eq!(Ok('b'), wall_map.get_char(2, 1));
        assert_eq!(Ok(true), wall_map.is_wall(2, 1));
        assert_eq!(Ok(' '), wall_map.get_char(3, 1));
        assert_eq!(Ok(false), wall_map.is_wall(3, 1));
        assert_eq!(Ok(' '), wall_map.get_char(4, 2));
        assert_eq!(Ok(false), wall_map.is_wall(4, 2));
        assert_eq!(Ok('6'), wall_map.get_char(5, 3));
        assert_eq!(Ok(true), wall_map.is_wall(5, 3));
        assert_eq!(Err(Error::InvalidXY((6, 4))), wall_map.get_char(6, 4));
        assert_eq!(Err(Error::InvalidXY((6, 4))), wall_map.is_wall(6, 4));
    }

    #[test]
    fn test_wall_map_2d_from_text_new_with_str_last_line() {
        let wall_map = WallMap2DFromText::new_with_str("aaaaaa\nb b b \n      \n123456\n");
        assert_eq!((6, 4), wall_map.shape());
    }

    #[test]
    fn test_wall_map_2d_from_text_new_with_str_various_len() {
        let wall_map = WallMap2DFromText::new_with_str("a\nb b b \n\n123456\n");
        assert_eq!((6, 4), wall_map.shape());
        assert_eq!(Ok('a'), wall_map.get_char(0, 0));
        assert_eq!(Ok(true), wall_map.is_wall(0, 0));
        assert_eq!(Ok('b'), wall_map.get_char(2, 1));
        assert_eq!(Ok(true), wall_map.is_wall(2, 1));
        assert_eq!(Ok(' '), wall_map.get_char(3, 1));
        assert_eq!(Ok(false), wall_map.is_wall(3, 1));
        assert_eq!(Ok(' '), wall_map.get_char(4, 2));
        assert_eq!(Ok(false), wall_map.is_wall(4, 2));
        assert_eq!(Ok('6'), wall_map.get_char(5, 3));
        assert_eq!(Ok(true), wall_map.is_wall(5, 3));
        assert_eq!(Err(Error::InvalidXY((6, 4))), wall_map.get_char(6, 4));
        assert_eq!(Err(Error::InvalidXY((6, 4))), wall_map.is_wall(6, 4));
    }
}

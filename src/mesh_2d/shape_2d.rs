// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

pub trait Shape2D {
    fn shape(&self) -> (usize, usize);
}

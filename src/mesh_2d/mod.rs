// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

//! The mesh_2d module contains mesh implementations in 2 dimensions.

mod compact_2d;
mod full_2d;
mod index_2d;
mod repr_2d;
mod shape_2d;
mod wall_map_2d;
mod wall_map_2d_from_text;

pub use compact_2d::*;
pub use full_2d::*;
pub use index_2d::*;
pub use repr_2d::*;
pub use shape_2d::*;
pub use wall_map_2d::*;
pub use wall_map_2d_from_text::*;

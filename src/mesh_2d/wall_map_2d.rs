// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

use crate::errors::*;
use crate::mesh_2d::shape_2d::*;

pub trait WallMap2D: Shape2D {
    fn is_wall(&self, x: usize, y: usize) -> Result<bool>;

    #[inline]
    fn is_free(&self, x: usize, y: usize) -> Result<bool> {
        match self.is_wall(x, y) {
            Ok(v) => Ok(!v),
            Err(e) => Err(e),
        }
    }
}

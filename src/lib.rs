// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

//! [Shortest Path](https://gitlab.com/ufoot/shortestpath) is an experimental library finding
//! the shortest path from A to B
//!
//! It aims primarily at powering [Liquid War 7](https://gitlab.com/ufoot/liquidwar7)
//! but could be of wider use. Who knows.
//!
//! ![Shortest Path icon](https://gitlab.com/ufoot/shortestpath/raw/main/shortestpath.png)

mod distance;
mod errors;
mod gate;
mod gradient;
mod mesh;

pub mod mesh_2d;
pub mod mesh_3d;
pub mod mesh_topo;

pub use distance::*;
pub use errors::*;
pub use gate::*;
pub use gradient::*;
pub use mesh::*;

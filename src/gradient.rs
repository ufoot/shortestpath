// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

use crate::distance::*;
use crate::gate::*;
use crate::mesh::*;
use std::fmt;
use std::iter::Iterator;

#[derive(Debug, Clone)]
pub struct Gradient {
    slots: Vec<Gate>,
}

impl Gradient {
    pub fn new_with_len(len: usize) -> Self {
        let mut slots = Vec::with_capacity(len);
        for i in 0..len {
            slots.push(Gate::new(i, DISTANCE_MAX));
        }
        Gradient { slots }
    }

    pub fn new_with_mesh(mesh: &impl Mesh) -> Self {
        Self::new_with_len(mesh.len())
    }

    pub fn spread_forward(&mut self, mesh: &impl Mesh, incr: f64) -> usize {
        if self.slots.len() != mesh.len() {
            panic!(
                "slots len {} does not match mesh len {}",
                self.slots.len(),
                mesh.len()
            )
        }
        (0..(mesh.len()))
            .into_iter()
            .map(|i| self.spread_slot(i, mesh, incr))
            .filter(|ok| *ok)
            .count()
    }

    pub fn spread_backward(&mut self, mesh: &impl Mesh, incr: f64) -> usize {
        if self.slots.len() != mesh.len() {
            panic!(
                "slots len {} does not match mesh len {}",
                self.slots.len(),
                mesh.len()
            )
        }
        (0..(mesh.len()))
            .rev()
            .into_iter()
            .map(|i| self.spread_slot(i, mesh, incr))
            .filter(|ok| *ok)
            .count()
    }

    pub fn spread_both(&mut self, mesh: &impl Mesh, incr: f64) -> usize {
        self.spread_forward(mesh, incr) + self.spread_backward(mesh, incr)
    }

    pub fn spread_slot(&mut self, here_index: usize, mesh: &impl Mesh, incr: f64) -> bool {
        let here = Gate::new(here_index, 0.0);
        let best = mesh.successors(here_index).into_iter().fold(here, |a, b| {
            if self.slots[a.target].distance + a.distance
                <= self.slots[b.target].distance + b.distance
            {
                a
            } else {
                b
            }
        });
        if best.target != here_index {
            self.slots[here_index] = Gate::new(
                best.target,
                self.slots[best.target].distance + best.distance,
            );
            true
        } else {
            // If nothing has changed, consider we're farther by "incr" which
            // avoids old paths to remain valid when they should not be.
            if incr != 0.0 {
                self.slots[here_index].distance += incr;
            }
            false
        }
    }

    pub fn incr(&mut self, incr: f64) {
        self.slots.iter_mut().for_each(|i| i.distance += incr);
    }

    pub fn spread(&mut self, mesh: &impl Mesh) {
        while self.spread_both(mesh, 0.0) > 0 {}
    }

    pub fn set_distance(&mut self, slot_index: usize, value: f64) {
        self.slots[slot_index].distance = value
    }

    pub fn get_distance(&self, slot_index: usize) -> f64 {
        self.slots[slot_index].distance
    }

    pub fn get_target(&self, slot_index: usize) -> usize {
        self.slots[slot_index].target
    }

    pub fn get(&self, slot_index: usize) -> &Gate {
        &self.slots[slot_index]
    }
}

impl std::fmt::Display for Gradient {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "TODO")
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::mesh_2d::*;

    #[test]
    fn test_spread_forward() {
        let rect = Full2D::new(5, 5);
        let mut grad = Gradient::new_with_mesh(&rect);
        grad.set_distance(12, 0.0);
        grad.spread_forward(&rect, 0.0);
        // row 0
        assert_eq!(DISTANCE_MAX, grad.get_distance(0));
        assert_eq!(0, grad.get_target(0));
        assert_eq!(DISTANCE_MAX, grad.get_distance(1));
        assert_eq!(1, grad.get_target(1));
        assert_eq!(DISTANCE_MAX, grad.get_distance(2));
        assert_eq!(2, grad.get_target(2));
        assert_eq!(DISTANCE_MAX, grad.get_distance(3));
        assert_eq!(3, grad.get_target(3));
        assert_eq!(DISTANCE_MAX, grad.get_distance(4));
        assert_eq!(4, grad.get_target(4));
        // row 1
        assert_eq!(DISTANCE_MAX, grad.get_distance(5));
        assert_eq!(5, grad.get_target(5));
        assert_eq!(DISTANCE_DIAGONAL, grad.get_distance(6));
        assert_eq!(12, grad.get_target(6));
        assert_eq!(DISTANCE_STRAIGHT, grad.get_distance(7));
        assert_eq!(12, grad.get_target(7));
        assert_eq!(DISTANCE_DIAGONAL, grad.get_distance(8));
        assert_eq!(12, grad.get_target(8));
        assert_eq!(DISTANCE_STRAIGHT + DISTANCE_DIAGONAL, grad.get_distance(9));
        assert_eq!(8, grad.get_target(9));
        // row 2
        assert_eq!(2.0 * DISTANCE_DIAGONAL, grad.get_distance(10));
        assert_eq!(6, grad.get_target(10));
        assert_eq!(DISTANCE_STRAIGHT, grad.get_distance(11));
        assert_eq!(12, grad.get_target(11));
        assert_eq!(DISTANCE_MIN, grad.get_distance(12));
        assert_eq!(12, grad.get_target(12));
        assert_eq!(DISTANCE_STRAIGHT, grad.get_distance(13));
        assert_eq!(12, grad.get_target(13));
        assert_eq!(2.0 * DISTANCE_STRAIGHT, grad.get_distance(14));
        assert_eq!(13, grad.get_target(14));
        // row 3
        assert_eq!(DISTANCE_STRAIGHT + DISTANCE_DIAGONAL, grad.get_distance(15));
        assert_eq!(11, grad.get_target(15));
        assert_eq!(DISTANCE_DIAGONAL, grad.get_distance(16));
        assert_eq!(12, grad.get_target(16));
        assert_eq!(DISTANCE_STRAIGHT, grad.get_distance(17));
        assert_eq!(12, grad.get_target(17));
        assert_eq!(DISTANCE_DIAGONAL, grad.get_distance(18));
        assert_eq!(12, grad.get_target(18));
        assert_eq!(DISTANCE_STRAIGHT + DISTANCE_DIAGONAL, grad.get_distance(19));
        assert_eq!(18, grad.get_target(19));
        // row 4
        assert_eq!(2.0 * DISTANCE_DIAGONAL, grad.get_distance(20));
        assert_eq!(16, grad.get_target(20));
        assert_eq!(DISTANCE_STRAIGHT + DISTANCE_DIAGONAL, grad.get_distance(21));
        assert_eq!(16, grad.get_target(21));
        assert_eq!(2.0 * DISTANCE_STRAIGHT, grad.get_distance(22));
        assert_eq!(17, grad.get_target(22));
        assert_eq!(DISTANCE_STRAIGHT + DISTANCE_DIAGONAL, grad.get_distance(23));
        assert_eq!(17, grad.get_target(23));
        assert_eq!(2.0 * DISTANCE_DIAGONAL, grad.get_distance(24));
        assert_eq!(18, grad.get_target(24));
    }

    #[test]
    fn test_spread_backward() {
        let rect = Full2D::new(5, 5);
        let mut grad = Gradient::new_with_mesh(&rect);
        grad.set_distance(12, 0.0);
        grad.spread_backward(&rect, 0.0);
        // row 5
        assert_eq!(DISTANCE_MAX, grad.get_distance(23));
        assert_eq!(24, grad.get_target(24));
        assert_eq!(DISTANCE_MAX, grad.get_distance(23));
        assert_eq!(23, grad.get_target(23));
        assert_eq!(DISTANCE_MAX, grad.get_distance(22));
        assert_eq!(22, grad.get_target(22));
        assert_eq!(DISTANCE_MAX, grad.get_distance(21));
        assert_eq!(21, grad.get_target(21));
        assert_eq!(DISTANCE_MAX, grad.get_distance(20));
        assert_eq!(20, grad.get_target(20));
        // row 4
        assert_eq!(DISTANCE_MAX, grad.get_distance(19));
        assert_eq!(19, grad.get_target(19));
        assert_eq!(DISTANCE_DIAGONAL, grad.get_distance(18));
        assert_eq!(12, grad.get_target(18));
        assert_eq!(DISTANCE_STRAIGHT, grad.get_distance(17));
        assert_eq!(12, grad.get_target(17));
        assert_eq!(DISTANCE_DIAGONAL, grad.get_distance(16));
        assert_eq!(12, grad.get_target(16));
        assert_eq!(DISTANCE_STRAIGHT + DISTANCE_DIAGONAL, grad.get_distance(15));
        assert_eq!(16, grad.get_target(15));
        // row 3
        assert_eq!(2.0 * DISTANCE_DIAGONAL, grad.get_distance(14));
        assert_eq!(18, grad.get_target(14));
        assert_eq!(DISTANCE_STRAIGHT, grad.get_distance(13));
        assert_eq!(12, grad.get_target(13));
        assert_eq!(DISTANCE_MIN, grad.get_distance(12));
        assert_eq!(12, grad.get_target(12));
        assert_eq!(DISTANCE_STRAIGHT, grad.get_distance(11));
        assert_eq!(12, grad.get_target(11));
        assert_eq!(2.0 * DISTANCE_STRAIGHT, grad.get_distance(10));
        assert_eq!(11, grad.get_target(10));
        // row 1
        assert_eq!(DISTANCE_STRAIGHT + DISTANCE_DIAGONAL, grad.get_distance(9));
        assert_eq!(13, grad.get_target(9));
        assert_eq!(DISTANCE_DIAGONAL, grad.get_distance(8));
        assert_eq!(12, grad.get_target(8));
        assert_eq!(DISTANCE_STRAIGHT, grad.get_distance(7));
        assert_eq!(12, grad.get_target(7));
        assert_eq!(DISTANCE_DIAGONAL, grad.get_distance(6));
        assert_eq!(12, grad.get_target(6));
        assert_eq!(DISTANCE_STRAIGHT + DISTANCE_DIAGONAL, grad.get_distance(5));
        assert_eq!(6, grad.get_target(5));
        // row 0
        assert_eq!(2.0 * DISTANCE_DIAGONAL, grad.get_distance(4));
        assert_eq!(8, grad.get_target(4));
        assert_eq!(DISTANCE_STRAIGHT + DISTANCE_DIAGONAL, grad.get_distance(3));
        assert_eq!(8, grad.get_target(3));
        assert_eq!(2.0 * DISTANCE_STRAIGHT, grad.get_distance(2));
        assert_eq!(7, grad.get_target(2));
        assert_eq!(DISTANCE_STRAIGHT + DISTANCE_DIAGONAL, grad.get_distance(1));
        assert_eq!(7, grad.get_target(1));
        assert_eq!(2.0 * DISTANCE_DIAGONAL, grad.get_distance(0));
        assert_eq!(6, grad.get_target(0));
    }

    #[test]
    fn test_spread() {
        let rect = Full2D::new(5, 5);
        let mut grad = Gradient::new_with_mesh(&rect);
        grad.set_distance(12, 0.0);
        grad.spread(&rect);
        // row 0
        assert_eq!(2.0 * DISTANCE_DIAGONAL, grad.get_distance(0));
        assert_eq!(6, grad.get_target(0));
        assert_eq!(DISTANCE_STRAIGHT + DISTANCE_DIAGONAL, grad.get_distance(1));
        assert_eq!(7, grad.get_target(1));
        assert_eq!(2.0 * DISTANCE_STRAIGHT, grad.get_distance(2));
        assert_eq!(7, grad.get_target(2));
        assert_eq!(DISTANCE_STRAIGHT + DISTANCE_DIAGONAL, grad.get_distance(3));
        assert_eq!(8, grad.get_target(3));
        assert_eq!(2.0 * DISTANCE_DIAGONAL, grad.get_distance(4));
        assert_eq!(8, grad.get_target(4));
        // row 1
        assert_eq!(DISTANCE_STRAIGHT + DISTANCE_DIAGONAL, grad.get_distance(5));
        assert_eq!(6, grad.get_target(5));
        assert_eq!(DISTANCE_DIAGONAL, grad.get_distance(6));
        assert_eq!(12, grad.get_target(6));
        assert_eq!(DISTANCE_STRAIGHT, grad.get_distance(7));
        assert_eq!(12, grad.get_target(7));
        assert_eq!(DISTANCE_DIAGONAL, grad.get_distance(8));
        assert_eq!(12, grad.get_target(8));
        assert_eq!(DISTANCE_STRAIGHT + DISTANCE_DIAGONAL, grad.get_distance(9));
        assert_eq!(8, grad.get_target(9));
        // row 2
        assert_eq!(2.0 * DISTANCE_STRAIGHT, grad.get_distance(10));
        assert_eq!(11, grad.get_target(10));
        assert_eq!(DISTANCE_STRAIGHT, grad.get_distance(11));
        assert_eq!(12, grad.get_target(11));
        assert_eq!(DISTANCE_MIN, grad.get_distance(12));
        assert_eq!(12, grad.get_target(12));
        assert_eq!(DISTANCE_STRAIGHT, grad.get_distance(13));
        assert_eq!(12, grad.get_target(13));
        assert_eq!(2.0 * DISTANCE_STRAIGHT, grad.get_distance(14));
        assert_eq!(13, grad.get_target(14));
        // row 3
        assert_eq!(DISTANCE_STRAIGHT + DISTANCE_DIAGONAL, grad.get_distance(15));
        assert_eq!(11, grad.get_target(15));
        assert_eq!(DISTANCE_DIAGONAL, grad.get_distance(16));
        assert_eq!(12, grad.get_target(16));
        assert_eq!(DISTANCE_STRAIGHT, grad.get_distance(17));
        assert_eq!(12, grad.get_target(17));
        assert_eq!(DISTANCE_DIAGONAL, grad.get_distance(18));
        assert_eq!(12, grad.get_target(18));
        assert_eq!(DISTANCE_STRAIGHT + DISTANCE_DIAGONAL, grad.get_distance(19));
        assert_eq!(18, grad.get_target(19));
        // row 4
        assert_eq!(2.0 * DISTANCE_DIAGONAL, grad.get_distance(20));
        assert_eq!(16, grad.get_target(20));
        assert_eq!(DISTANCE_STRAIGHT + DISTANCE_DIAGONAL, grad.get_distance(21));
        assert_eq!(16, grad.get_target(21));
        assert_eq!(2.0 * DISTANCE_STRAIGHT, grad.get_distance(22));
        assert_eq!(17, grad.get_target(22));
        assert_eq!(DISTANCE_STRAIGHT + DISTANCE_DIAGONAL, grad.get_distance(23));
        assert_eq!(17, grad.get_target(23));
        assert_eq!(2.0 * DISTANCE_DIAGONAL, grad.get_distance(24));
        assert_eq!(18, grad.get_target(24));
    }
}

// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

use crate::distance::*;

#[derive(Debug, Clone)]
pub struct Gate {
    pub target: usize,
    pub distance: f64,
}

impl Gate {
    pub fn new(target: usize, distance: f64) -> Self {
        Self { target, distance }
    }
}

impl PartialEq for Gate {
    fn eq(&self, other: &Gate) -> bool {
        if self.target != other.target {
            return false;
        }
        let diff = f64::abs(self.distance - other.distance);
        diff < DISTANCE_PRECISION
    }
}

impl Eq for Gate {}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]

    fn test_eq() {
        let a = Gate::new(1, 1.0);
        let b = Gate::new(1, 1.000_001);
        let c = Gate::new(1, 1.000_000_000_001);
        let d = Gate::new(2, 1.0);

        assert_eq!(a, a);
        assert_eq!(b, b);
        assert_eq!(c, c);
        assert_eq!(c, c);
        assert_ne!(a, b);
        assert_eq!(a, c);
        assert_ne!(a, d);
    }
}

// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

use std::f64::consts::SQRT_2;

pub const DISTANCE_MIN: f64 = 0.0;
pub const DISTANCE_MAX: f64 = f64::MAX;
pub const DISTANCE_STRAIGHT: f64 = 1.0;
pub const DISTANCE_DIAGONAL: f64 = SQRT_2;

pub(crate) const DISTANCE_PRECISION: f64 = 1e-9;

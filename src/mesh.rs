// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

// from https://docs.rs/pathfinding/4.8.0/pathfinding/directed/dijkstra/fn.dijkstra.html
//
// pub fn dijkstra<N, C, FN, IN, FS>(
//     start: &N,
//     successors: FN,
//     success: FS
// ) -> Option<(Vec<N>, C)>
// where
//     N: Eq + Hash + Clone,
//     C: Zero + Ord + Copy,
//     FN: FnMut(&N) -> IN,
//     IN: IntoIterator<Item = (N, C)>,
//     FS: FnMut(&N) -> bool,

use crate::gate::*;
use std::iter::Iterator;

pub trait Mesh {
    type IntoIter: Iterator<Item = Gate>;

    fn successors(&self, from: usize) -> Self::IntoIter;
    fn len(&self) -> usize;
}

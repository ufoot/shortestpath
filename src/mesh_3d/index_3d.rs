// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

use crate::errors::*;
use crate::mesh_3d::shape_3d::*;

pub trait Index3D: Shape3D {
    fn index_to_xyz(&self, index: usize) -> Result<(usize, usize, usize)>;
    fn xyz_to_index(&self, x: usize, y: usize, z: usize) -> Result<usize>;
}

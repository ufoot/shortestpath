// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

use crate::errors::*;
use crate::gate::*;
use crate::mesh::*;
use crate::mesh_3d::full_3d::*;
use crate::mesh_3d::index_3d::*;
use crate::mesh_3d::shape_3d::*;
use crate::mesh_3d::wall_map_3d::*;
use crate::mesh_3d::wall_map_3d_from_text::*;

#[derive(Debug, Clone)]
pub struct Compact3D {
    width: usize,
    height: usize,
    depth: usize,
    full_to_compact: Vec<usize>,
    compact_to_full: Vec<usize>,
    successors_map: Vec<Vec<Gate>>,
}

impl Compact3D {
    pub fn new_full(width: usize, height: usize, depth: usize) -> Self {
        let size = width * height * depth;
        let mut full_to_compact = Vec::with_capacity(size);
        let mut successors_map = Vec::with_capacity(size);
        for i in 0..size {
            full_to_compact.push(i);
            let successors = Full3D::successors(width, height, depth, i).collect();
            successors_map.push(successors);
        }
        let compact_to_full = full_to_compact.clone();
        Self {
            width,
            height,
            depth,
            full_to_compact,
            compact_to_full,
            successors_map,
        }
    }

    pub fn new_with_wall_map(wall_map: &impl WallMap3D) -> Self {
        let (width, height, depth) = wall_map.shape();
        let size = width * height * depth;
        let mut full_to_compact = Vec::with_capacity(size);
        let mut compact_to_full = Vec::with_capacity(size);

        let mut current_full = 0;
        let mut current_compact = 0;
        for z in 0..depth {
            for y in 0..height {
                for x in 0..width {
                    if wall_map.is_free(x, y, z).unwrap() {
                        compact_to_full.push(current_full);
                        full_to_compact.push(current_compact);
                        current_full += 1;
                        current_compact += 1;
                    } else {
                        full_to_compact.push(size);
                        current_full += 1;
                    }
                }
            }
        }
        let mut successors_map = Vec::with_capacity(compact_to_full.len());
        for i in 0..compact_to_full.len() {
            let full_index = compact_to_full[i];
            let possible_successors = Full3D::successors(width, height, depth, full_index);
            let successors = possible_successors
                .filter(|s| {
                    // keep only gates opening to some place that is possible
                    let (x, y, z) = Full3D::index_to_xyz(width, height, depth, s.target).unwrap();
                    wall_map.is_free(x, y, z).unwrap()
                })
                .map(|s| Gate {
                    // translate the full index to a compact index
                    distance: s.distance,
                    target: full_to_compact[s.target],
                })
                .collect();
            successors_map.push(successors);
        }

        Self {
            width,
            height,
            depth,
            full_to_compact,
            compact_to_full,
            successors_map,
        }
    }

    pub fn new_with_str(input: &str) -> Self {
        let wall_map = WallMap3DFromText::new_with_str(input);
        Self::new_with_wall_map(&wall_map)
    }

    pub fn new_with_string(input: &String) -> Self {
        let wall_map = WallMap3DFromText::new_with_string(input);
        Self::new_with_wall_map(&wall_map)
    }

    pub fn new_with_vec_str(input: &Vec<&str>) -> Self {
        let wall_map = WallMap3DFromText::new_with_vec_str(input);
        Self::new_with_wall_map(&wall_map)
    }

    pub fn new_with_vec_string(input: &Vec<String>) -> Self {
        let wall_map = WallMap3DFromText::new_with_vec_string(input);
        Self::new_with_wall_map(&wall_map)
    }

    pub fn new_with_iter_str<'a, I>(input: I) -> Self
    where
        I: Iterator<Item = &'a str>,
    {
        let wall_map = WallMap3DFromText::new_with_iter_str(input);
        Self::new_with_wall_map(&wall_map)
    }

    pub fn new_with_iter_string<'a, I>(input: I) -> Self
    where
        I: Iterator<Item = &'a String>,
    {
        let wall_map = WallMap3DFromText::new_with_iter_string(input);
        Self::new_with_wall_map(&wall_map)
    }

    fn compact_to_full_index(&self, compact_index: usize) -> Result<usize> {
        if compact_index >= self.compact_to_full.len() {
            return Err(Error::invalid_index(compact_index));
        }
        let full_index = self.compact_to_full[compact_index];
        if full_index >= self.full_to_compact.len() {
            return Err(Error::invalid_index(full_index));
        }
        Ok(full_index)
    }

    fn full_to_compact_index(&self, full_index: usize) -> Result<usize> {
        if full_index >= self.full_to_compact.len() {
            return Err(Error::invalid_index(full_index));
        }
        let compact_index = self.full_to_compact[full_index];
        if compact_index >= self.compact_to_full.len() {
            return Err(Error::invalid_index(compact_index));
        }
        Ok(compact_index)
    }
}

impl Mesh for Compact3D {
    type IntoIter = std::vec::IntoIter<Gate>;
    fn successors(&self, from: usize) -> std::vec::IntoIter<Gate> {
        self.successors_map[from].clone().into_iter()
    }

    fn len(&self) -> usize {
        self.compact_to_full.len()
    }
}

impl Index3D for Compact3D {
    fn index_to_xyz(&self, index: usize) -> Result<(usize, usize, usize)> {
        let full_index = self.compact_to_full_index(index)?;
        Full3D::index_to_xyz(self.width, self.height, self.depth, full_index)
    }

    fn xyz_to_index(&self, x: usize, y: usize, z: usize) -> Result<usize> {
        let full_index = Full3D::xyz_to_index(self.width, self.height, self.width, x, y, z)?;
        self.full_to_compact_index(full_index)
    }
}

impl Shape3D for Compact3D {
    fn shape(&self) -> (usize, usize, usize) {
        (self.width, self.height, self.depth)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::gradient::*;
    use crate::mesh_3d::repr_3d::*;

    #[test]
    fn test_compact_3d_full_basic() {
        let mesh = Compact3D::new_full(4, 3, 2);
        assert_eq!((4, 3, 2), mesh.shape());
        assert_eq!(24, mesh.len());
        let mut grad = Gradient::new_with_mesh(&mesh);
        grad.set_distance(0, 0.0);
        grad.spread(&mesh);
        assert_eq!(
            String::from("0123\n1123\n2234\n----\n1234\n2234\n3345\n----\n"),
            repr_mesh_with_gradient_3d(&mesh, &grad)
        );
    }

    #[test]
    fn test_compact_3d_with_str_small() {
        let mesh = Compact3D::new_with_str(" ## \n    \n#  #\n====\n####\n\n\n====");
        assert_eq!((4, 3, 2), mesh.shape());
        assert_eq!(16, mesh.len());
        let mut grad = Gradient::new_with_mesh(&mesh);
        assert_eq!(
            String::from("?##?\n????\n#??#\n----\n####\n????\n????\n----\n"),
            repr_mesh_with_gradient_3d(&mesh, &grad)
        );
        grad.set_distance(0, 0.0);
        grad.spread(&mesh);
        assert_eq!(
            String::from("0##4\n1123\n#23#\n----\n####\n2234\n3345\n----\n"),
            repr_mesh_with_gradient_3d(&mesh, &grad)
        );
    }
}

// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

//! The mesh_3d module contains mesh implementations in 3 dimensions.

mod compact_3d;
mod full_3d;
mod index_3d;
mod repr_3d;
mod shape_3d;
mod wall_map_3d;
mod wall_map_3d_from_text;

pub use compact_3d::*;
pub use full_3d::*;
pub use index_3d::*;
pub use repr_3d::*;
pub use shape_3d::*;
pub use wall_map_3d::*;
pub use wall_map_3d_from_text::*;

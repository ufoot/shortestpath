// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

use crate::errors::*;
use crate::mesh_3d::shape_3d::*;

pub trait WallMap3D: Shape3D {
    fn is_wall(&self, x: usize, y: usize, z: usize) -> Result<bool>;

    #[inline]
    fn is_free(&self, x: usize, y: usize, z: usize) -> Result<bool> {
        match self.is_wall(x, y, z) {
            Ok(v) => Ok(!v),
            Err(e) => Err(e),
        }
    }
}

// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

use crate::errors::*;
use crate::mesh_2d::*;
use crate::mesh_3d::shape_3d::*;
use crate::mesh_3d::wall_map_3d::*;

pub struct WallMap3DFromText {
    width: usize,
    height: usize,
    data: Vec<Vec<Vec<char>>>,
}

impl WallMap3DFromText {
    fn width_height_from_data(data: &Vec<Vec<Vec<char>>>) -> (usize, usize) {
        let mut width = 0;
        let mut height = 0;
        for plane in data {
            if plane.len() > height {
                height = plane.len();
            }
            for line in plane {
                if line.len() > width {
                    width = line.len();
                }
            }
        }

        (width, height)
    }

    pub fn get_char(&self, x: usize, y: usize, z: usize) -> Result<char> {
        if z >= self.data.len() {
            return Err(Error::invalid_xyz(x, y, z));
        }
        if y >= self.height {
            return Err(Error::invalid_xyz(x, y, z));
        }
        if x >= self.width {
            return Err(Error::invalid_xyz(x, y, z));
        }
        let plane = &self.data[z];
        if y >= plane.len() {
            // If undef, it's not a wall, one can go there
            return Ok(WallMap2DFromText::DEFAULT_FREE_CHAR);
        }
        let line = &plane[y];
        if x >= line.len() {
            // If undef, it's not a wall, one can go there
            return Ok(WallMap2DFromText::DEFAULT_FREE_CHAR);
        }
        Ok(line[x])
    }

    pub(crate) fn is_wall_char(chr: char) -> bool {
        WallMap2DFromText::is_wall_char(chr)
    }

    pub(crate) fn is_sep_line(line: &str) -> bool {
        let mut has_sep_char = false;
        for chr in line.chars() {
            if WallMap2DFromText::is_sep_char(chr) {
                has_sep_char = true;
            } else {
                // Tolerating spaces, they are easy to forget.
                // Not using DEFAULT_SEP_CHAR here as we're really
                // tracking ' ' as "a typo space" and not our separator.
                if chr != ' ' {
                    return false;
                }
            }
        }
        has_sep_char
    }

    pub fn new_with_str(input: &str) -> WallMap3DFromText {
        Self::new_with_iter_str(input.lines())
    }

    pub fn new_with_string(input: &String) -> WallMap3DFromText {
        Self::new_with_iter_str(input.lines())
    }

    pub fn new_with_vec_str(input: &Vec<&str>) -> WallMap3DFromText {
        Self::new_with_iter_str(input.iter().map(|l| *l))
    }

    pub fn new_with_vec_string(input: &Vec<String>) -> WallMap3DFromText {
        Self::new_with_iter_string(input.iter())
    }

    pub fn new_with_iter_str<'a, I>(input: I) -> WallMap3DFromText
    where
        I: Iterator<Item = &'a str>,
    {
        let mut data: Vec<Vec<Vec<char>>> = Vec::new();
        let mut plane_data: Vec<Vec<char>> = Vec::new();
        for line in input {
            if Self::is_sep_line(line) {
                data.push(plane_data);
                plane_data = Vec::new();
            } else {
                let line_data = String::from(line).chars().collect();
                plane_data.push(line_data);
            }
        }
        if plane_data.len() > 0 {
            data.push(plane_data);
        }
        let (width, height) = Self::width_height_from_data(&data);
        WallMap3DFromText {
            width,
            height,
            data,
        }
    }

    pub fn new_with_iter_string<'a, I>(input: I) -> WallMap3DFromText
    where
        I: Iterator<Item = &'a String>,
    {
        Self::new_with_iter_str(input.map(|l| l.as_str()))
    }
}

impl WallMap3D for WallMap3DFromText {
    fn is_wall(&self, x: usize, y: usize, z: usize) -> Result<bool> {
        let chr = self.get_char(x, y, z)?;
        Ok(Self::is_wall_char(chr))
    }
}

impl Shape3D for WallMap3DFromText {
    fn shape(&self) -> (usize, usize, usize) {
        (self.width, self.height, self.data.len())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_wall_map_3d_from_text_is_sep_line() {
        assert!(WallMap3DFromText::is_sep_line("-"));
        assert!(WallMap3DFromText::is_sep_line("__"));
        assert!(WallMap3DFromText::is_sep_line("== = "));
        assert!(!WallMap3DFromText::is_sep_line(""));
        assert!(!WallMap3DFromText::is_sep_line("abc"));
        assert!(!WallMap3DFromText::is_sep_line("_x_"));
        assert!(!WallMap3DFromText::is_sep_line("."));
        assert!(!WallMap3DFromText::is_sep_line("  "));
    }

    #[test]
    fn test_wall_map_3d_from_text_new_with_str_basic() {
        let wall_map =
            WallMap3DFromText::new_with_str("aaaaaa\nb b b \n      \n123456\n------\n\n xyz\n");
        assert_eq!((6, 4, 2), wall_map.shape());
        assert_eq!(Ok('a'), wall_map.get_char(0, 0, 0));
        assert_eq!(Ok(true), wall_map.is_wall(0, 0, 0));
        assert_eq!(Ok('b'), wall_map.get_char(2, 1, 0));
        assert_eq!(Ok(true), wall_map.is_wall(2, 1, 0));
        assert_eq!(Ok(' '), wall_map.get_char(3, 1, 0));
        assert_eq!(Ok(false), wall_map.is_wall(3, 1, 0));
        assert_eq!(Ok(' '), wall_map.get_char(4, 2, 0));
        assert_eq!(Ok(false), wall_map.is_wall(4, 2, 0));
        assert_eq!(Ok('6'), wall_map.get_char(5, 3, 0));
        assert_eq!(Ok(true), wall_map.is_wall(5, 3, 0));
        assert_eq!(Ok(' '), wall_map.get_char(0, 0, 1));
        assert_eq!(Ok(false), wall_map.is_wall(0, 0, 1));
        assert_eq!(Ok('x'), wall_map.get_char(1, 1, 1));
        assert_eq!(Ok(true), wall_map.is_wall(1, 1, 1));
        assert_eq!(
            Err(Error::InvalidXYZ((6, 4, 2))),
            wall_map.get_char(6, 4, 2)
        );
        assert_eq!(Err(Error::InvalidXYZ((6, 4, 2))), wall_map.is_wall(6, 4, 2));
    }

    #[test]
    fn test_wall_map_3d_from_text_new_with_str_eof() {
        let wall_map = WallMap3DFromText::new_with_str("a\nabc\n====================\n");
        assert_eq!((3, 2, 1), wall_map.shape());
    }
}

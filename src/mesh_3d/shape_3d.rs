// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

pub trait Shape3D {
    fn shape(&self) -> (usize, usize, usize);
}

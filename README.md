# Shortest Path

[Shortest Path](https://gitlab.com/ufoot/shortestpath) is an experimental library finding
the shortest path from A to B, implemented in [Rust](https://www.rust-lang.org/).

It aims primarily at powering [Liquid War 7](https://gitlab.com/ufoot/liquidwar7)
but could be of wider use. Who knows.

![Shortest Path icon](https://gitlab.com/ufoot/shortestpath/raw/main/shortestpath.png)

# Status

For now this is a toy project, clearly *NOT* suitable for production use.

[![Build Status](https://gitlab.com/ufoot/shortestpath/badges/main/pipeline.svg)](https://gitlab.com/ufoot/shortestpath/pipelines)

# Usage

[TODO...]

# History

Technically, this is a variant of [Dijkstra's Algorithm](https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm).

A famous, classical algorithm which we re-invented (as in, re-invent the wheel) with my friend Thomas Colcombet back in 1995.
Back in those days we did not have access to Internet and never stumbled on that great work by Dijkstra but
somehow managed to use its main idea. First code snippets in [Liquid War 3](https://gitlab.com/ufoot/liquidwar3).

This implementation tries to make it independant from the [Liquid War game](https://ufoot.org/liquidwar/) and
offer a multi-purpose version. It still aims at speed execution rather than exactness, in the context of
many agents trying to find the shortest point to a single target.

# Similar packages

The [pathfinding](https://crates.io/crates/pathfinding) crate has a multi-purpose,
very likely stricter version of this, along with many other path finding algorithms.

It has been a great source of inspiration.

# License

Shortest Path is licensed under the [MIT](https://gitlab.com/ufoot/shortestpath/blob/master/LICENSE) license.
